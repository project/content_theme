

Module
======
Content Theme


Description
===========
This module allows to use different themes than the site default theme on
content creating, editing, and viewing pages.


Requirements
============
Drupal 7.x


Installation
============
1. Move this folder into your modules directory.
2. Enable it from Administer >> Modules >> Theme related.


Configuration
=============
* Configure the content wide theme at Administer >> Structure >>
  Content theme >> Content wide.
* Configure content type themes in its content type settings or at Administer >>
  Structure >> Content theme >> Content type.
* Configure content node themes in its content node settings or at Administer >>
  Structure >> Content theme >> Content node.


Projekt page
============
http://drupal.org/project/content_theme


Author & maintainer
===================
Ralf Stamm


License
=======
GNU General Public License (GPL)
